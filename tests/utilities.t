name: utilities-1
description:
	Check basename
category: debian,posix
stdin:
	basename /
	basename //////
	basename /surrounded/
	basename /triple/decker/
	basename /suffix/test/test.suffix
	basename /suffix/test/test.suffix suffix
expected-stdout:
	/
	/
	surrounded
	decker
	test.suffix
	test.
---
name: utilities-2
description:
	Check break
category: debian,posix
stdin:
	while true
	do
	  echo one in
	  while true
	  do
	    echo two in
	    while true
	    do
	      echo three in
	      break 3
	    done
	  done
	done
	
	echo broke out
expected-stdout:
	one in
	two in
	three in
	broke out
---
name: utilities-3
description:
	Check cat
category: debian,posix
stdin:
	cat /dev/null
	cat -u /dev/null
	echo catted
expected-stdout:
	catted
---
name: utilities-4
description:
	Check cd
category: debian,posix
stdin:
	cd /
	pwd
	cd -L /
	pwd
	cd -P /
	pwd
	cd -
expected-stdout:
	/
	/
	/
	/
---
name: utilities-5
description:
	Check colon
category: debian,posix
stdin:
	: ${TESTPARAMETER=lalala}
	if false
	then
	:
	else
	echo $TESTPARAMETER
	fi
expected-stdout:
	lalala
---
name: utilities-6
description:
	Check command
category: debian,posix
stdin:
	echo --version
	command echo --version
	command -p echo --version
	command -v echo
	command -V echo
expected-stdout:
	--version
	--version
	--version
	echo
	shell builtin: echo
---
name: utilities-echo-1
description:
	Check POSIX echo behavior
category: debian,posix
expected-fail: yes
stdin:
	echo echo madness
	echo -n madness
	echo 'even more\nmadness\nwith echo\tright\040\c'
	echo -- here
expected-stdout:
	echo madness
	-n madness
	even more
	madness
	with echo	right -- here
---
name: utilities-echo-2
description:
	Check Debian echo behavior
category: debian
stdin:
	echo echo madness
	echo -n madness
	echo 'even more\nmadness\nwith echo\tright\040\c'
	echo -- here
expected-stdout:
	echo madness
	madnesseven more
	madness
	with echo	right -- here
---
name: utilities-7
description:
	Check eval
category: debian,posix
stdin:
	eval TST=$(echo test)
	echo $TST
expected-stdout:
	test
---


name: utilities-8
description:
	Check umask
category: debian,posix
stdin:
	umask 222
	umask
	umask g+w
	umask
	umask -- -rw
	umask
	umask -rw
	umask
expected-stdout:
	0222
	0202
	0666
	0666
expected-stderr-pattern: /umask: invalid option -- 'r'/
---

name: utilities-9
description:
	Check unset
category: debian,posix
stdin:
	BLAH=blah
	unset FOO BAR BLAH
	unset FOO BAR BLAH
	unset FOO BAR BLAH
	unset -f echo
	echo $FOO $BAR $BLAH
expected-stdout:
	
---
name: utilities-10
description:
	Check test with invalid values
category: debian,posix
stdin:
	test asdf -ge 0
expected-stdout:
expected-stderr-pattern: /.*unexpected .end of expression./
expected-exit: 1

---
