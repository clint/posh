
name: trap-2
description:
	Check if EXIT trap is executed for sub shells.
stdin:
	trap 'echo parent exit' EXIT
	echo start
	(echo A; echo A last)
	echo B
	(echo C; trap 'echo sub exit' EXIT; echo C last)
	echo parent last
expected-stdout: 
	start
	A
	A last
	B
	C
	C last
	sub exit
	parent last
	parent exit
---

name: trap-3
description:
	Check that trap errors when an action is given with no conditions
stdin:
	trap 0
expected-stderr-pattern:
	/.*no signals.*/
expected-exit: e != 0
---

name: trap-4
description:
	Check that trap errors when a lowercase signal name is given
stdin:
	trap 'echo hi' exit
expected-stderr-pattern:
	/.*bad signal.*/
expected-exit: e != 0
---

