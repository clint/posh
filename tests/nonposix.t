name: nonposix-1
description:
	Check to see if local can take multiple arguments and assignments
category: debian
stdin:
	local a=1 b=5 c=9
	echo $a $b $c
expected-stdout:
	1 5 9
---
