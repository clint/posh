# These tests concern special variables.
#
name: ifs-1
description:
	Check that IFS is either reset or inherited.
category: posix
stdin:
	defaultifs=' 	
	' IFS=, "$__progname" -c '
		test "$IFS" = , ||
		test "$IFS" = "$defaultifs"
	'
expected-exit: 0
---
name: ifs-2
category: posix
description:
	Check that unset IFS is either overridden or left alone.
	unset IFS
	defaultifs=' 	
	' "$__progname" -c '
		test "${IFS-unset}" = unset ||
		test "$IFS" = "$defaultifs"
	'
expected-exit: 0
---
name: ifs-3
description:
	Check that IFS is inherited.
category: posh
stdin:
	IFS=, "$__progname" -c 'test "$IFS" = ,'
expected-exit: 0
---
name: ifs-4
description:
	Check that IFS is reset.
category: ksh
stdin:
	IFS=, "$__progname" -c 'test "$IFS" = " 	
	"'
expected-exit: 0
---
