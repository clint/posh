# These tests deal with quoting.
#
name: error-1
description:
	Check that failure to find a command returns the proper
	exit status.
category: debian,posix
stdin:
	(ridiculouscommandname) 2>/dev/null || echo $?
expected-stdout:
	127
---
name: error-2
description:
	Check that failure to execute a command returns the proper
	exit status.
category: debian,posix
stdin:
	TEMPFILE=$(tempfile --prefix posix)
	chmod 0 $TEMPFILE
	($TEMPFILE) 2>/dev/null || echo $?
	rm -f $TEMPFILE
expected-stdout:
	126
---
name: error-3
description:
	POSIX says that the shell should exit with 126/127 in some situations
category: debian,posix
stdin:
	i=0
	echo : >x
	"${__progname}" ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	"${__progname}" -c ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	echo exit 42 >x
	"${__progname}" ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	"${__progname}" -c ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	rm -f x
	"${__progname}" ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	"${__progname}" -c ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	mkdir x
	"${__progname}" ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	"${__progname}" -c ./x >/dev/null 2>&1; r=$?; echo $((i++)) $r .
	rmdir x
expected-stdout:
	0 0 .
	1 126 .
	2 42 .
	3 126 .
	4 127 .
	5 127 .
	6 126 .
	7 126 .
---

