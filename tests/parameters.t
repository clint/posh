# These tests deal with parameters
#
name: parameters-1
description:
	Check setting of positional parameters
category: debian,posix
stdin:
	set -- To comply or not to comply.  That is the question.
	echo 1: $1
	echo 2: $2
	echo 3: $3
	echo 4: $4
	echo 5: $5
	echo 6: $6
	echo 7: $7
	echo 8: $8
	echo 9: $9
	echo 10: $10
	echo 10 again, with feeling: ${10}
expected-stdout:
	1: To
	2: comply
	3: or
	4: not
	5: to
	6: comply.
	7: That
	8: is
	9: the
	10: To0
	10 again, with feeling: question.
---
name: parameters-2
description:
	Check expansion of special parameters
category: debian,posix
stdin:
	set -- To comply or not to comply.  That was the question.
	echo $@
	echo "$@"
	echo $*
	echo "$*"
	echo This should be 10: $#.
	echo Now once more with IFS changed.
	IFS=":"
	echo $@
	echo "$@"
	echo $*
	echo "$*"
	echo That was exciting.
	echo "Exit status: $?"
expected-stdout:
	To comply or not to comply. That was the question.
	To comply or not to comply. That was the question.
	To comply or not to comply. That was the question.
	To comply or not to comply. That was the question.
	This should be 10: 10.
	Now once more with IFS changed.
	To comply or not to comply. That was the question.
	To comply or not to comply. That was the question.
	To comply or not to comply. That was the question.
	To:comply:or:not:to:comply.:That:was:the:question.
	That was exciting.
	Exit status: 0
---
name: parameters-3
description:
	Check PWD parameter
category: debian,posix
stdin:
	cd /
	pwd
	/bin/pwd
	echo $PWD
expected-stdout:
	/
	/
	/
---
name: parameters-4
description:
	Check parameter expansions
category: debian,posix
stdin:
	FULLPARAMETER="yes"
	NULLPARAMETER=""
	echo ${FULLPARAMETER:-one}
	echo ${EMPTYPARAMETER:-two}
	echo ${NULLPARAMETER:-three}
	echo ${FULLPARAMETER-four}
	echo ${EMPTYPARAMETER-five}
	echo ${NULLPARAMETER-six}
	echo ${FULLPARAMETER:=seven}
	echo ${EMPTYPARAMETER:=eight}
	echo ${FULLPARAMETER:+nine}
	echo ${EMPTYPARAMETER:+ten}
	echo ${NEWEMPTYPARAMETER:+eleven}
	echo ${NULLPARAMETER:+twelve}
	echo ${FULLPARAMETER+thirteen}
	echo ${EMPTYPARAMETER+fourteen}
	echo ${NEWEMPTYPARAMETER+fifteen}
	echo ${NULLPARAMETER?seventeen}
	echo ${FULLPARAMETER:?eighteen}
	echo ${EMPTYPARAMETER:?nineteen}
expected-stdout:
	yes
	two
	three
	yes
	five
	
	yes
	eight
	nine
	ten
	
	
	thirteen
	fourteen
	
	
	yes
	eight
---
name: parameters-5
description:
	Check parameter expansion substrings
category: debian,posix
stdin:
	TESTSTRING="abcDEF"
	echo ${TESTSTRING%[A-Z]?}
	echo ${TESTSTRING%%[A-Z]?}
	echo ${TESTSTRING#[a-z]?}
	echo ${TESTSTRING##[a-z]?}
expected-stdout:
	abcD
	abcD
	cDEF
	cDEF
---
