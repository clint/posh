# These tests deal with quoting.
#
name: quoting-1
description:
	Check that quoting by means of backslash
category: debian,posix
stdin:
	echo \|\&\;\<\>\(\)\$\`\\\"\'\ \	\
	This a continuation of the echo line.
expected-stdout:
	|&;<>()$`\"' 	This a continuation of the echo line.
---
name: quoting-2
description:
	Check single-quoting
category: debian,posix
stdin:
	FULLVARIABLE="Two words"
	echo 'One $EMPTYVARIABLE'
	echo 'Two ${EMPTYVARIABLE}'
	echo '$(echo cmdsubst $FULLVARIABLE)'
	echo '`echo backquot $FULLVARIABLE`'
expected-stdout:
	One $EMPTYVARIABLE
	Two ${EMPTYVARIABLE}
	$(echo cmdsubst $FULLVARIABLE)
	`echo backquot $FULLVARIABLE`
---
name: quoting-3
description:
	Check double-quoting
category: debian,posix
stdin:
	FULLVARIABLE="Two words"
	echo "One $EMPTYVARIABLE"
	echo "Two ${EMPTYVARIABLE}"
	echo "$(echo cmdsubst $FULLVARIABLE)"
	echo "`echo backquot $FULLVARIABLE`"
expected-stdout:
	One 
	Two 
	cmdsubst Two words
	backquot Two words
---
name: blsk-nl-ign-5
description:
	Check what happens with backslash at end of input
	(the old bourne shell trashes them; so do we)
stdin: !
	echo `echo foo\\`bar
	echo hi\
expected-stdout:
	foobar
	hi
---
name: blsk-nl-10
description:
	Check that \newline in a keyword is collapsed
stdin: 
	i\
	f true; then\
	 echo pass; el\
	se echo fail; fi
expected-stdout:
	pass
---
