name: exec-simple-1
description:
	Assignments for the execution of a function should not be kept.
category: debian,posix
stdin:
	f() { echo $a; }
	a=1 f
	echo x$a
expected-stdout:
	1
	x
---

name: exec-negate-1
description:
	A negated command is compound and should not trigger exit with set -e
category: debian,posix
stdin:
	set -e
	! true
	echo OK
expected-stdout:
	OK
---

